//
//  PlaceCell.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {
  
  @IBOutlet weak var imgAvatar: UIImageView!
  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblTime: UILabel!
  @IBOutlet weak var lblNumberOfUsers: UILabel!
  @IBOutlet weak var lblDistance: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    imgAvatar.layer.borderWidth = 2
    imgAvatar.layer.borderColor = UIColor.white.cgColor
  }
  
  func showUserAvatar(named name: String) {
    imgAvatar.image = UIImage(named: name)
  }
  
}

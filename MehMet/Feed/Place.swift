//
//  Place.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import Foundation

struct Place {
  let name: String
  let userAvatar: String
  let numberOfUsers: Int
  let distance: Int
  let hours: Int
}

//
//  FeedViewController.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {

  var presenter: FeedPresenter?

  //MARK: - IBOutlets
  
  @IBOutlet weak var tblPlaces: UITableView!
  
  //MARK: - UIViewControllelr overrides
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    FeedConfiguratorImplementation.configure(self)
    presenter?.viewDidLoad()
  }

}

//MARK: - FeedPresenter

extension FeedViewController: FeedView {
  func reloadData() {
    tblPlaces.reloadData()
  }
}

//MARK: - UITableViewDataSource

extension FeedViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter?.numberOfPlaces() ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier.place.rawValue, for: indexPath) as! PlaceCell
    presenter?.configure(cell: cell, for: indexPath.row)
    return cell
  }
}

//MARK: - UITableViewDelegate

extension FeedViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    defer { tableView.deselectRow(at: indexPath, animated: true)}
    presenter?.cellDidSelected(with: indexPath.row)
  }
}













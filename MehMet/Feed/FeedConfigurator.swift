//
//  FeedConfigurator.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import Foundation

protocol FeedConfigurator {
  static func configure(_ feedController: FeedViewController)
}

class FeedConfiguratorImplementation {}

extension FeedConfiguratorImplementation: FeedConfigurator {
  static func configure(_ feedController: FeedViewController) {
    let router = FeedRouterImplemetation(view: feedController)
    let presenter = FeedPresenterImplementation(view: feedController, router: router)
    feedController.presenter = presenter
  }
}

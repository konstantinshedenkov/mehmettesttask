//
//  FeedPresenter.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import Foundation

protocol FeedView: class {
  func reloadData()
}

protocol FeedPresenter {
  
  func viewDidLoad() 
  
  func numberOfPlaces() -> Int
  func configure(cell: PlaceCell, for index: Int)
  func cellDidSelected(with index: Int)
}

protocol FeedRouter {
  func openPlace(with place: Place)
}

class FeedPresenterImplementation {
  
  fileprivate weak var view: FeedView?
  fileprivate let router: FeedRouter
  
  fileprivate var places = [Place]()
  
  init(view: FeedView, router: FeedRouter) {
    self.view = view
    self.router = router
  }
  
  fileprivate func getPlaces() {
    FakeGenerator.getRandomPlaces(count: 10) { (places) in
      self.places += places
      self.view?.reloadData()
    }
  }
  
}

//MARK: - FeedPresenter

extension FeedPresenterImplementation: FeedPresenter {
  
  func viewDidLoad() {
    getPlaces()
  }
  
  func numberOfPlaces() -> Int {
    return places.count
  }
  
  func configure(cell: PlaceCell, for index: Int) {
    guard places.count > index else { return }
    let place = places[index]
    
    // Clear image. For example: before async downloading
    cell.imgAvatar.image = nil
    cell.showUserAvatar(named: place.userAvatar)
    
    cell.lblName.text = place.name
    cell.lblNumberOfUsers.text = "\(place.numberOfUsers)"
    cell.lblDistance.text = "\(place.distance) KM"
  }
  
  func cellDidSelected(with index: Int) {
    guard places.count > index else { return }
    let place = places[index]
    
    router.openPlace(with: place)
  }
}














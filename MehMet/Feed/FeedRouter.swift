//
//  FeedRouter.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import Foundation

class FeedRouterImplemetation {
  fileprivate weak var view: FeedViewController?
  
  init(view: FeedViewController) {
    self.view = view
  }
}

extension FeedRouterImplemetation: FeedRouter {
  func openPlace(with place: Place) {
    if let placeController = view?.storyboard?.instantiateViewController(withIdentifier: "PlaceViewController") as? PlaceViewController {
      placeController.place = place
      view?.navigationController?.show(placeController, sender: view)
    }
  }
}

//
//  FakeGenerator.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import Foundation

class FakeGenerator {
  
  private class func randomPlace() -> Place {
    
    let randomInt = arc4random() % 10000
    let randomNumberOfUsers = Int(arc4random() % 15)
    let randomDistance = Int(arc4random() % 99 + 1)
    let randomHours = Int(arc4random() % 100 + 24)
    
    let place = Place(name: "Place #\(randomInt)", userAvatar: "Bitmap", numberOfUsers: randomNumberOfUsers, distance: randomDistance, hours: randomHours)
    
    return place
  }
  
  public class func getRandomPlaces(count: Int, completion: (([Place]) -> ())) {
    var result = [Place]()
    for _ in 0..<count {
      result.append(randomPlace())
    }
    completion(result)
  }
  
  public class func getVehicles(completion: (([Vehicle]) -> ())) {
    var result = [Vehicle]()
    result.append(Vehicle(image: "bus", name: "Bus", time: "18:16"))
    result.append(Vehicle(image: "train", name: "Train", time: "20:20"))
    result.append(Vehicle(image: "plane", name: "Airplane", time: "22:35"))
    completion(result)
  }
}

//
//  PlaceViewController.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import UIKit

class PlaceViewController: UIViewController {
  
  @IBOutlet weak var tblVehicles: UITableView!
  
  var presenter: PlacePresenter?
  var place: Place?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    prepareTable()
    
    PlaceConfiguratorImplementation.configure(self, with: place)
    presenter?.viewDidLoad()
  }
  
  private func prepareTable() {
    tblVehicles.estimatedRowHeight = 100.0
    tblVehicles.rowHeight = UITableViewAutomaticDimension
  }
  
  @IBAction func btnBackPressed(_ sender: UIBarButtonItem) {
    navigationController?.popViewController(animated: true)
  }
}

//MARK: - PlaceView
extension PlaceViewController: PlaceView {
  func reloadData() {
    tblVehicles.reloadData()
  }
  
  func showTitle(_ title: String) {
    self.title = title
  }
}

//MARK: - UITableViewDataSource
extension PlaceViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter?.numberOfVehicles() ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let vehicleCell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier.vehicle.rawValue, for: indexPath) as! VehicleCell
    presenter?.configure(vehicleCell, for: indexPath.row)
    return vehicleCell
  }
}

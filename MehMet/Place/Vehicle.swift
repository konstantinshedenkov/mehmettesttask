//
//  Vehicle.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import Foundation

struct Vehicle {
  let image: String
  let name: String
  let time: String
}

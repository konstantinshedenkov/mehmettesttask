//
//  VehicleCell.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import UIKit

class VehicleCell: UITableViewCell {
  
  @IBOutlet weak var topLineView: UIView!
  @IBOutlet weak var imgVehicle: UIImageView!
  @IBOutlet weak var lblVehicleName: UILabel!
  @IBOutlet weak var lblTime: UILabel!
  @IBOutlet weak var txtDescription: UITextView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  func showVehicleImage(named name: String) {
    imgVehicle.image = UIImage(named: name)
  }
  
  
}

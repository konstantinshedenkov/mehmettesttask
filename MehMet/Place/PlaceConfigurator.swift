//
//  PlaceConfigurator.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import Foundation

protocol PlaceConfigurator {
  static func configure(_ placeController: PlaceViewController, with place: Place?)
}

class PlaceConfiguratorImplementation {}

extension PlaceConfiguratorImplementation: PlaceConfigurator {
  static func configure(_ placeController: PlaceViewController, with place: Place?) {
    let presenter = PlacePresenterImplementation(view: placeController, place: place)
    placeController.presenter = presenter
  }
}

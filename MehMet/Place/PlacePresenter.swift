//
//  PlacePresenter.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import Foundation

protocol PlaceView: class {
  func reloadData()
  func showTitle(_ title: String)
}

protocol PlacePresenter {
  func viewDidLoad()
  func numberOfVehicles() -> Int
  func configure(_ cell: VehicleCell, for index: Int)
}

protocol PlaceRouter {
  
}

class PlacePresenterImplementation {
  fileprivate weak var view: PlaceView?
  
  let place: Place?
  var vehicles = [Vehicle]()
  
  init(view: PlaceView, place: Place?) {
    self.view = view
    self.place = place
  }
  
  fileprivate func getVehicles() {
    FakeGenerator.getVehicles { (vehicles) in
      self.vehicles += vehicles
      self.view?.reloadData()
    }
  }
}

//MARK: - PlacePresenter

extension PlacePresenterImplementation: PlacePresenter {
  
  func viewDidLoad() {
    if let name = place?.name {
      view?.showTitle(name)
    }
    
    getVehicles()
  }
  
  func numberOfVehicles() -> Int {
    return vehicles.count
  }
  
  func configure(_ cell: VehicleCell, for index: Int) {
    guard vehicles.count > index else { return }
    let vehicle = vehicles[index]
    
    cell.topLineView.isHidden = index == 0
    
    cell.showVehicleImage(named: vehicle.image)
    cell.lblVehicleName.text = vehicle.name
    cell.lblTime.text = vehicle.time
  }
}

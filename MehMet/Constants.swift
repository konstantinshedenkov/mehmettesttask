//
//  Constants.swift
//  MehMet
//
//  Created by iDeveloper on 17.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import Foundation

enum Cell {
  enum identifier: String {
    case place = "PlaceCell"
    case vehicle = "VehicleCell"
  }
}
